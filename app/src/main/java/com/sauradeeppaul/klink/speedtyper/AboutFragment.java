package com.sauradeeppaul.klink.speedtyper;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * Created by sauradeep on 07/03/17.
 */
public class AboutFragment extends Fragment {

    static AboutFragment fragment;
    public static final String TAG = "about";

    static MainActivity activity;

    TextView rateText, versionText;

    public static AboutFragment newInstance() {
        fragment = new AboutFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(getActivity() instanceof MainActivity)
            activity=(MainActivity) getActivity();
        View view = inflater.inflate(R.layout.about, container, false);

        rateText = (TextView) view.findViewById(R.id.rateButton);
        versionText = (TextView) view.findViewById(R.id.version);

        handleTextBehaviour();
        return view;
    }

    private void handleTextBehaviour(){
        rateText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("market://details?id=" + activity.getPackageName());
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
                    goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                            Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                            Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                } else{
                    goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                            Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                }
                try {
                    startActivity(goToMarket);
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id=" + activity.getPackageName())));
                }
            }
        });

        try {
            String version = "Version " + String.valueOf(activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0).versionName);
            versionText.setText(version);
        } catch (Exception e){
            e.printStackTrace();
            versionText.setVisibility(View.GONE);
        }
    }
}
