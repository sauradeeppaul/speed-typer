package com.sauradeeppaul.klink.speedtyper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sauradeep on 05/03/17.
 */
public class BigFirstElementAdapter extends ArrayAdapter<String> {

    LayoutInflater inflater;
    List<String> words;

    public BigFirstElementAdapter(Context context, int resource, List<String> objects) {
        super(context, resource, objects);
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        words = objects;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (position == 0) {
            ViewHolder holder = null;
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.big_element_row, null);
                holder = new ViewHolder();
                holder.textView = (TextView)convertView.findViewById(R.id.text);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder)convertView.getTag();
            }
            holder.textView.setText(words.get(position));
        } else {
            ViewHolder holder = null;
            if (convertView == null) {
                convertView = inflater.inflate(android.R.layout.simple_list_item_1, null);
                holder = new ViewHolder();
                holder.textView = (TextView)convertView.findViewById(android.R.id.text1);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder)convertView.getTag();
            }
            holder.textView.setText(words.get(position));
        }
        return convertView;
    }

    public static class ViewHolder {
        public TextView textView;
    }
}
