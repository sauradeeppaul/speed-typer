package com.sauradeeppaul.klink.speedtyper;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;


/**
 * Created by sauradeep on 04/03/17.
 */
public class GameActivity extends ParentActivity{

    final static int LIST_SIZE = 3000;

    private static boolean pause = true;

    public static boolean showPauseDialog = true;
    public boolean continueMusic = true;;

    public static GameActivity current_activity;

    public static boolean UNMUTED = true;

    private static GameEndDialog ged;
    private static GamePausedDialog gpd;

    public static int roundsSinceAds = 0;

    public final static String TAG = "Game_Activity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getActionBar() != null)
            getActionBar().hide();
        setContentView(R.layout.activity_main);

        getWindow().getDecorView().setBackgroundColor(Color.WHITE);

        showPauseDialog = true;

        current_activity = this;

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            UNMUTED = extras.getBoolean("unmuted");
        }

        Utils.launchFragment(this, GameFragment.newInstance(this), MenuFragment.TAG);

        MainActivity.isGameOn = true;
    }

    @Override
    protected void onDestroy() {
        MainActivity.isGameOn = false;
        showPauseDialog = false;

        if(gpd != null)
            gpd.dismiss();

        if(ged != null)
            ged.dismiss();

        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (UNMUTED && MusicManager.isPlaying())
            MusicManager.pause();
//        }
//        MusicManager.release();
    }
    @Override
    protected void onResume() {
        super.onResume();
        continueMusic = false;

        if(UNMUTED) {
            if (MusicManager.isInitialized() && MusicManager.getCurrentSong() != MusicManager.Music.MUSIC_GAME)
                MusicManager.release();
            MusicManager.start(this, MusicManager.Music.MUSIC_GAME);
        }
    }

    @Override
    public void onBackPressed() {
        handlePauseOrQuit();
    }


    public void handlePauseOrQuit(){
        pauseGame();

        if(showPauseDialog) {
            try {
                gpd = new GamePausedDialog(this);
                gpd.setCancelable(false);
                gpd.show();
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public static void handleGameEnd(int wordScore, int letterScore){
        showPauseDialog = false;

        roundsSinceAds ++;
        pauseGame();

        MainActivity.updateLeaderboard(wordScore, letterScore);

        try {
            ged = new GameEndDialog(GameActivity.current_activity, wordScore, letterScore);
            ged.setCancelable(false);
            ged.show();
        } catch(Exception e){
            e.printStackTrace();
        }

        if(roundsSinceAds >= 2){
            if(mInterstitialAd != null && mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
                roundsSinceAds = 0;
            }
        }
    }

    public void share(int score){
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");

        String string = "Hey! I just scored " + score + " in Speed Typer!\n" +
                "Let's see how you score!\n" +
                "Download the game at http://play.google.com/store/apps/details?id=" + getPackageName();

        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, string);
        startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.share_via)));
    }

    public static void pauseGame(){
        pause = true;
        GameFragment.getInstance().enableDisableTextBox(false);
        if(GameFragment.getInstance() != null)
            GameFragment.getInstance().cancelPauseTimer();
    }

    public void resumeGame(){
        pause = false;
        GameFragment.getInstance().enableDisableTextBox(true);
    }

    public void onKeyboardClick(View view){
        GameFragment.getInstance().handleKeyboardClick(view);
    }

    public boolean isGamePaused(){
        return pause;
    }
}
