package com.sauradeeppaul.klink.speedtyper;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

/**
 * Created by sauradeep on 06/03/17.
 */
public class GameEndDialog extends Dialog implements
        android.view.View.OnClickListener {

    public GameActivity activity;
    public Dialog d;
    public TextView share, restart, end, endScore, letterEndScore, lettersSingPlural;

    int wordScore, letterScore;

    public GameEndDialog (GameActivity a, int wordScore, int letterScore) {
        super(a);
        // TODO Auto-generated constructor stub
        this.activity = a;
        this.wordScore = wordScore;
        this.letterScore = letterScore;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.end_dialog);
        restart = (TextView) findViewById(R.id.restart);
        share = (TextView) findViewById(R.id.share);
        end = (TextView) findViewById(R.id.end_game);
        endScore = (TextView) findViewById(R.id.end_score);
        letterEndScore = (TextView) findViewById(R.id.end_letter_score);
        lettersSingPlural = (TextView) findViewById(R.id.letter_sing_plural);

        endScore.setText(String.valueOf(wordScore));
        letterEndScore.setText(" " + String.valueOf(letterScore) + " ");

        if(letterScore==1)
            lettersSingPlural.setText("letter!");
        else
            lettersSingPlural.setText("letters!");

        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        restart.setOnClickListener(this);
        share.setOnClickListener(this);
        end.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {


        switch (v.getId()) {
            case R.id.share:
                activity.share(wordScore);
                break;
            case R.id.restart:
                dismiss();

                if(GameFragment.getInstance() != null)
                    GameFragment.getInstance().startNewGame();
                break;
            case R.id.end_game:
                dismiss();

                if(GameFragment.getInstance() != null)
                    GameFragment.getInstance().quitGame();
                break;
        }
    }
}
