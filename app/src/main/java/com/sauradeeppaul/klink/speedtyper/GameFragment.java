package com.sauradeeppaul.klink.speedtyper;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by sauradeep on 04/03/17.
 */
public class GameFragment extends Fragment {

    static GameFragment fragment;
    TextView textBox;
    static GameActivity activity;
    public static final String TAG = "game";
    BigFirstElementAdapter arrayAdapter;
    int wordScore = 0;
    int letterScore = 0;
    int time = 60000;

    boolean isInputEnabled = false;

    Animation timerAnimation;

    int pauseTimer = 3;

    TextView wordScoreText, letterScoreText, timeText, pauseTimerText, timeOverText;
    AdView mAdView;

    Timer t, pt;

    ListView wordList;
    List<String> wordsInView = new ArrayList<>();

    public static GameFragment newInstance(GameActivity act) {
        fragment = new GameFragment();
        if(activity == null)
            activity = act;
        return fragment;
    }

    public static GameFragment getInstance (){
        return fragment;
    }

    private String currentWord = null, textBoxString = "";

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.game_view, container, false);
        textBox = (TextView) view.findViewById(R.id.text_box);
        wordList = (ListView) view.findViewById(R.id.word_list);

        wordScoreText = (TextView) view.findViewById(R.id.word_score);
        letterScoreText = (TextView) view.findViewById(R.id.letter_score);
        timeText = (TextView) view.findViewById(R.id.time);

        pauseTimerText = (TextView) view.findViewById(R.id.pause_timer);
        timeOverText = (TextView) view.findViewById(R.id.time_over);

        mAdView = (AdView) view.findViewById(R.id.game_ad);

        loadAds();
        initAnimation();

        startNewGame();

        return view;
    }

    public void startNewGame(){
        GameActivity.pauseGame();
        GameActivity.showPauseDialog = true;

        if(pt != null)
            cancelPauseTimer();

        if(t != null)
            t.cancel();

        fetchAndPopulateList();

        textBoxString = "";
        textBox.setText(textBoxString);

        timeText.setTextColor(ContextCompat.getColor(activity, R.color.colorGreen));

        wordScore = 0;
        letterScore = 0;
        wordScoreText.setText(String.valueOf(wordScore));
        letterScoreText.setText(String.valueOf(letterScore));

        time = 60000;

        initTimer();

        resumeGame();
    }

    private void endGame(){
        GameActivity.showPauseDialog = false;
        GameActivity.pauseGame();

        timeOverText.setVisibility(View.VISIBLE);

        Animation timeOverAnimation = AnimationUtils.loadAnimation(activity, R.anim.game_over_entry);
        timeOverText.clearAnimation();
        timeOverText.setAnimation(timeOverAnimation);
        timeOverAnimation.start();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                timeOverText.setVisibility(View.GONE);
                GameActivity.handleGameEnd(wordScore, letterScore);
            }
        }, 2000);

    }

    public void quitGame(){
        GameActivity.showPauseDialog = false;
        if(t != null)
            t.cancel();

        if(pt != null)
            cancelPauseTimer();

        activity.finish();
    }

    private void loadAds(){

        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);
    }

    private void initTimer(){
        t = new Timer();

        t.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                if(activity != null) {
                    activity.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            if (time % 1000 == 0)
                                timeText.setText(String.valueOf(time / 1000));

                            if (time <= 10000 && time > 5000) {
                                timeText.setTextColor(ContextCompat.getColor(activity, R.color.colorYellow));
                            } else if (time <= 5000)
                                timeText.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));

                            if (time == 0) {
                                t.cancel();
                                endGame();
                            }

                            if(time < 0){
                                handleGameError();
                            }

                            if (activity != null && !activity.isGamePaused())
                                time -= 1;
                        }

                    });
                } else {
                    t.cancel();
                    handleGameError();
                }
            }

        }, 0, 1);
    }

    private void handleGameError(){
        Toast.makeText(activity, getResources().getString(R.string.error_in_game), Toast.LENGTH_SHORT).show();
        quitGame();
    }

    private void fetchAndPopulateList(){
        Random random = new Random();

        wordsInView = new ArrayList<>();

        for(int i = 0; i<10; ++i){
            wordsInView.add(activity.words.get(random.nextInt(GameActivity.LIST_SIZE-2)));
        }

//        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(
//                activity,
//                android.R.layout.simple_list_item_1,
//                wordsInView );

        arrayAdapter = new BigFirstElementAdapter(
                activity,
                android.R.layout.simple_list_item_1,
                wordsInView);

        wordList.setAdapter(arrayAdapter);

        wordList.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_MOVE) {
                    return true;
                }
                return false;
            }
        });

        getFirstWord();
    }

    private void getFirstWord(){
        String word = wordsInView.get(0).toLowerCase();

        String firstWord = "";
        for(char c : word.toCharArray()){
            if(Character.isLetter(c))
                firstWord = firstWord + c;
        }
        currentWord = firstWord;
    }

    public void resumeGame(){
        activity.showPauseDialog = true;

        pt = new Timer();
        pauseTimer = 4;

        pt.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                pauseTimer--;

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        animateTimer();
                    }
                });;

                if(pauseTimer == 0 && pt != null)
                    cancelPauseTimer();

                if(pauseTimer < 0)
                    handleGameError();
            }

        }, 200, 1000);
    }

    private void initAnimation(){
        timerAnimation = AnimationUtils.loadAnimation(activity, R.anim.pause_timer);
        pauseTimerText.setAnimation(timerAnimation);
    }

    public void animateTimer(){
        if(pauseTimer == 3)
            pauseTimerText.setVisibility(View.VISIBLE);

        if(pauseTimer == 0) {
            pauseTimerText.setText(getResources().getString(R.string.go));
            pauseTimerText.setTextColor(ContextCompat.getColor(activity, R.color.colorGreen));
        }
        else {
            pauseTimerText.setText(String.valueOf(pauseTimer));
            pauseTimerText.setTextColor(ContextCompat.getColor(activity, R.color.colorBlack));
        }
        pauseTimerText.startAnimation(timerAnimation);

        timerAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if(pauseTimer == 0) {
                    pauseTimerText.setVisibility(View.GONE);
                    activity.resumeGame();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public void cancelPauseTimer(){
        try {
            if(pt != null)
                pt.cancel();
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    pauseTimerText.setVisibility(View.GONE);
                }
            });
        } catch (Exception e){
            e.printStackTrace();
        }
    }



    @Override
    public void onPause(){
        activity.handlePauseOrQuit();
        super.onPause();
    }

    @Override
    public void onDestroy(){
        if(t!=null)
            t.cancel();
        if(pt != null)
            cancelPauseTimer();
        activity = null;
        super.onDestroy();
    }

    public void enableDisableTextBox(final boolean enable){
        isInputEnabled = enable;
    }

    public void handleKeyboardClick(View view){
        if(!isInputEnabled)
            return;

        Button button = (Button) view;
        if(isLetter(button)){
            textBox.setText(enterLetter(button));
            handleTextChange(textBoxString, true);
        } else if (isSpace(button)){
            if(!MainActivity.AUTO_SWITCH)
                handleLineBreak(textBoxString, true);
            else{
                textBox.setText(enterLetter(button));
                handleTextChange(textBoxString, true);
            }
        } else if (isReturn(button)){
            handleLineBreak(textBoxString, false);
        } else if(isPause(button)){
            activity.handlePauseOrQuit();
        } else if(isDelete(button)){
            if(textBoxString.length()>0) {
                textBox.setText(deleteLetter());
                handleTextChange(textBoxString, false);
            }
        }
    }

    private void handleTextChange(String s, boolean playFx){
        if(MainActivity.AUTO_SWITCH && currentWord.equals(s)) {
                textBox.setBackgroundColor(ContextCompat.getColor(activity, R.color.colorAccent));
                switchWord();
        }
        else {
            if (currentWord.startsWith(s)) {
                textBox.setBackgroundColor(ContextCompat.getColor(activity, R.color.colorAccent));
            } else {
                textBox.setBackgroundColor(ContextCompat.getColor(activity, R.color.colorRed));
                if(playFx && MainActivity.UNMUTED_FX)
                    MusicManager.playAudioFX(activity, MusicManager.FX.WRONG_MOVE);
            }
        }
    }

    private void handleLineBreak(String s, boolean isSpacebar){
        if(s.substring(0, s.length()).equals(currentWord))
            switchWord();
        else {
            if (isSpacebar){
                textBoxString = textBoxString + " ";
                textBox.setText(textBoxString);
            }
            handleTextChange(textBoxString, true);
        }
    }

    private void switchWord(){
        wordsInView.remove(0);
        Random random = new Random();
        wordsInView.add(MainActivity.words.get(random.nextInt(GameActivity.LIST_SIZE-2)));
        if(arrayAdapter != null)
            arrayAdapter.notifyDataSetChanged();

        letterScore = letterScore + currentWord.length();

        getFirstWord();

        textBoxString = "";
        textBox.setText(textBoxString);

        wordScore ++;
        wordScoreText.setText(String.valueOf(wordScore));
        letterScoreText.setText(String.valueOf(letterScore));

    }

    private String enterLetter(Button button) {
        textBoxString = textBoxString + button.getText().toString();
        return textBoxString;
    }

    private String deleteLetter(){
        textBoxString = textBoxString.substring(0, textBoxString.length() - 1);
        return textBoxString;
    }

    private boolean isLetter(Button button){
        if(isSpace(button) || isPause(button) || isDelete(button) || isReturn(button))
            return false;
        else
            return true;
    }

    private boolean isSpace(Button button){
        if(button.getId() == R.id.Spacestr)
            return true;
        else
            return false;
    }

    private boolean isPause(Button button){
        if(button.getId() == R.id.Pausestr)
            return true;
        else
            return false;
    }

    private boolean isReturn(Button button){
        if(button.getId() == R.id.Returnstr)
            return true;
        else
            return false;
    }

    private boolean isDelete(Button button){
        if(button.getId() == R.id.Backstr)
            return true;
        else
            return false;
    }
}
