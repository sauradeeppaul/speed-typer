package com.sauradeeppaul.klink.speedtyper;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

/**
 * Created by sauradeep on 06/03/17.
 */
public class GamePausedDialog extends Dialog implements
        android.view.View.OnClickListener {

    public GameActivity activity;
    public Dialog d;
    public TextView resume, restart, end;

    public GamePausedDialog (GameActivity a) {
        super(a);
        // TODO Auto-generated constructor stub
        this.activity = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.pause_dialog);
        restart = (TextView) findViewById(R.id.restart);
        resume = (TextView) findViewById(R.id.resume);
        end = (TextView) findViewById(R.id.end_game);

        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        restart.setOnClickListener(this);
        resume.setOnClickListener(this);
        end.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        dismiss();

        switch (v.getId()) {
            case R.id.resume:
                if(GameFragment.getInstance() != null)
                    GameFragment.getInstance().resumeGame();
                break;
            case R.id.restart:
                if(GameFragment.getInstance() != null)
                    GameFragment.getInstance().startNewGame();
                break;
            case R.id.end_game:
                if(GameFragment.getInstance() != null)
                    GameFragment.getInstance().quitGame();
                break;
        }
    }
}
