package com.sauradeeppaul.klink.speedtyper;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by sauradeep on 07/03/17.
 */
public class HelpFragment extends Fragment {

    static HelpFragment fragment;
    public static final String TAG = "help";

    static MainActivity activity;

    TextView rateText, benSoundsText;

    public static HelpFragment newInstance() {
        fragment = new HelpFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(getActivity() instanceof MainActivity)
            activity=(MainActivity) getActivity();
        View view = inflater.inflate(R.layout.help, container, false);

        return view;
    }
}
