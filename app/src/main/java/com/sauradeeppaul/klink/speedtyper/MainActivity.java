package com.sauradeeppaul.klink.speedtyper;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.achievement.Achievements;
import com.google.android.gms.games.leaderboard.LeaderboardVariant;
import com.google.android.gms.games.leaderboard.Leaderboards;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends ParentActivity implements GoogleApiClient.OnConnectionFailedListener{

    final static String WORDS_LEADERBOARD_ID = "CgkIzv6bl7YHEAIQAA";
    final static String LETTERS_LEADERBOARD_ID = "CgkIzv6bl7YHEAIQCA";

    final static String ACHIEVEMENT_ID_BORED = "CgkIzv6bl7YHEAIQAw";
    final static String ACHIEVEMENT_ID_THUMB_TAPPER = "CgkIzv6bl7YHEAIQBA";
    final static String ACHIEVEMENT_ID_FLASH = "CgkIzv6bl7YHEAIQBQ";
    final static String ACHIEVEMENT_ID_SLOTH = "CgkIzv6bl7YHEAIQBg";
    final static String ACHIEVEMENT_ID_FASTER_THAN_LIGHT = "CgkIzv6bl7YHEAIQBw";
    final static String ACHIEVEMENT_ID_DROWNED = "CgkIzv6bl7YHEAIQCQ";
    final static String ACHIEVEMENT_ID_TEXT_CHAMPION = "CgkIzv6bl7YHEAIQCg";
    final static String ACHIEVEMENT_ID_SLOWPOKE= "CgkIzv6bl7YHEAIQCw";

    static boolean isUserSignedIn = false, continueMusic = true;

    public final static int RC_SIGN_IN = 100;
    public final static int RC_LEADERBOARD = 101;
    public final static int RC_ACHIEVEMENTS = 102;

    public String name = null;

    static SharedPreferences preferences;
    public static final String MY_PREFERENCES = "MyPrefs";

    public static boolean UNMUTED = true;
    public static boolean AUTO_SWITCH = true;
    public static boolean UNMUTED_FX = true;
    Timer interstitialAdTimer;

    public static MainActivity mainActivity;

    ProgressDialog mProgressDialog;

    static GoogleApiClient mGoogleApiClient;

    static final int INTERSTITIAL_AD_PERIOD = 200000;
    static boolean showAd = false, isGameOn = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getActionBar() != null)
            getActionBar().hide();
        setContentView(R.layout.activity_main);

        if(words != null && words.size() == SplashFragment.FILE_SIZE)
            Utils.launchFragment(this, MenuFragment.newInstance(), MenuFragment.TAG);
        else
            Utils.launchFragment(this, SplashFragment.newInstance(), SplashFragment.TAG);

        initializeAds();

        mainActivity = this;

        preferences = getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        UNMUTED = preferences.getBoolean("unmuted", true);
        UNMUTED_FX = preferences.getBoolean("fx_on", true);
        AUTO_SWITCH = preferences.getBoolean("auto_switch", true);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestProfile()
                .build();

        if(mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .enableAutoManage(this, this)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .addApi(Games.API)
                    .addScope(Games.SCOPE_GAMES)
                    .build();
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        mGoogleApiClient.connect(GoogleApiClient.SIGN_IN_MODE_OPTIONAL);

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            Log.d("MainActivity", "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        } else {

            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    handleSignInResult(googleSignInResult);
                }
            });
        }

        if(UNMUTED) {
            if (MusicManager.isInitialized() && MusicManager.getCurrentSong() != MusicManager.Music.MUSIC_MENU)
                MusicManager.release();
            MusicManager.start(this, MusicManager.Music.MUSIC_MENU);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
        if (UNMUTED && MusicManager.isPlaying() && MusicManager.getCurrentSong() == MusicManager.Music.MUSIC_MENU)
            MusicManager.pause();
    }

    public static void setUNMUTED(boolean UNMUTED) {
        MainActivity.UNMUTED = UNMUTED;
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("unmuted", UNMUTED).apply();
    }

    public static void setUNMUTED_FX(boolean UNMUTED_FX) {
        MainActivity.UNMUTED_FX = UNMUTED_FX;
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("fx_on", UNMUTED_FX).apply();
    }

    public static void setAutoSwitch(boolean AUTO_SWITCH) {
        MainActivity.AUTO_SWITCH = AUTO_SWITCH;
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("auto_switch", AUTO_SWITCH).apply();
    }

    private void  initializeAds(){
        MobileAds.initialize(getApplicationContext(), "ca-app-pub-8083747030187453~9336800722");

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getResources().getString(R.string.interstitial_ad_unit_id));

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();
            }
        });

        interstitialAdTimer = new Timer();

        interstitialAdTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showAd = true;

                        if(mInterstitialAd.isLoaded() && !isGameOn) {
                            mInterstitialAd.show();
                            showAd = false;
                        }
                    }
                });
            }

        }, INTERSTITIAL_AD_PERIOD, INTERSTITIAL_AD_PERIOD);

        requestNewInterstitial();
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .build();

        mInterstitialAd.loadAd(adRequest);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("Google API Client", "Failed");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
            isUserSignedIn = true;
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("MainActivity", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            isUserSignedIn = true;

            if(acct != null)
                name = acct.getDisplayName();

            if(Utils.getCurrentFragment(this) instanceof StatsFragment) {
                ((StatsFragment) Utils.getCurrentFragment(this)).updateUI(name, true);
            }

            hideProgressDialog();

//            mStatusTextView.setText(getString(R.string.signed_in_fmt, acct.getDisplayName()));
//            updateUI(true);
        } else {

            hideProgressDialog();
            Toast.makeText(this, getResources().getString(R.string.error_signing_in), Toast.LENGTH_SHORT).show();
            // Signed out, show unauthenticated UI.
//            updateUI(false);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
//        if (MusicManager.isPlaying())
//            MusicManager.pause();
//        MusicManager.release();

    }
    @Override
    protected void onResume() {
        super.onResume();
        continueMusic = false;

        if(mInterstitialAd.isLoaded() && showAd && !isGameOn){
            mInterstitialAd.show();
            showAd = false;
        }
//
//        if(UNMUTED) {
//            if (MusicManager.isInitialized() && MusicManager.getCurrentSong() != MusicManager.Music.MUSIC_MENU)
//                MusicManager.release();
//            MusicManager.start(this, MusicManager.Music.MUSIC_MENU);
//        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(UNMUTED && MusicManager.isInitialized())
            MusicManager.release();

        if(UNMUTED_FX)
            MusicManager.releaseFxPlayer();
    }

    public static void updateLeaderboard(final int score, final int letterScore){
        try {

            if(isUserSignedIn) {
                Games.Leaderboards.loadCurrentPlayerLeaderboardScore(mGoogleApiClient,
                        WORDS_LEADERBOARD_ID,
                        LeaderboardVariant.TIME_SPAN_ALL_TIME,
                        LeaderboardVariant.COLLECTION_PUBLIC).setResultCallback(new ResultCallback<Leaderboards.LoadPlayerScoreResult>() {
                    @Override
                    public void onResult(@NonNull Leaderboards.LoadPlayerScoreResult loadPlayerScoreResult) {

                        Games.Leaderboards.submitScore(mGoogleApiClient,
                                WORDS_LEADERBOARD_ID,
                                score);
                    }
                });

                Games.Leaderboards.loadCurrentPlayerLeaderboardScore(mGoogleApiClient,
                        LETTERS_LEADERBOARD_ID,
                        LeaderboardVariant.TIME_SPAN_ALL_TIME,
                        LeaderboardVariant.COLLECTION_PUBLIC).setResultCallback(new ResultCallback<Leaderboards.LoadPlayerScoreResult>() {
                    @Override
                    public void onResult(@NonNull Leaderboards.LoadPlayerScoreResult loadPlayerScoreResult) {

                        Games.Leaderboards.submitScore(mGoogleApiClient,
                                LETTERS_LEADERBOARD_ID,
                                letterScore);
                    }
                });

                Games.Achievements.load(mGoogleApiClient, true).setResultCallback(new ResultCallback<Achievements.LoadAchievementsResult>() {
                    @Override
                    public void onResult(@NonNull Achievements.LoadAchievementsResult loadAchievementsResult) {

                        if (score == 0)
                            Games.Achievements.unlock(mGoogleApiClient, ACHIEVEMENT_ID_SLOTH);
                        if (score >= 10)
                            Games.Achievements.unlock(mGoogleApiClient, ACHIEVEMENT_ID_BORED);
                        if (score >= 20)
                            Games.Achievements.unlock(mGoogleApiClient, ACHIEVEMENT_ID_THUMB_TAPPER);
                        if (score >= 30)
                            Games.Achievements.unlock(mGoogleApiClient, ACHIEVEMENT_ID_FLASH);
                        if (score >= 50)
                            Games.Achievements.unlock(mGoogleApiClient, ACHIEVEMENT_ID_FASTER_THAN_LIGHT);
                        if(letterScore >= 200)
                            Games.Achievements.unlock(mGoogleApiClient, ACHIEVEMENT_ID_DROWNED);
                        if(letterScore >= 500)
                            Games.Achievements.unlock(mGoogleApiClient, ACHIEVEMENT_ID_TEXT_CHAMPION);
                        if(letterScore <= 50)
                            Games.Achievements.unlock(mGoogleApiClient, ACHIEVEMENT_ID_SLOWPOKE);

                    }
                });
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        MusicManager.release();
//    }
//
    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {

            if (keyCode == KeyEvent.KEYCODE_BACK) {
                new AlertDialog.Builder(this)
                        .setTitle(R.string.quit_tile)
                        .setMessage(R.string.quit_msg)
                        .setPositiveButton(R.string.quit, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                                System.exit(0);
                            }
                        })
                        .setNegativeButton(R.string.cancel, null)
                        .show();
                return true;
            }

        }
        return super.onKeyDown(keyCode, event);
    }
}
