package com.sauradeeppaul.klink.speedtyper;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

/**
 * Created by sauradeep on 04/03/17.
 */
public class MenuFragment extends Fragment {

    static MenuFragment fragment;
    static Activity activity;
    public static final String TAG = "menu";

    RelativeLayout playButton, statsButton, settingsButton, aboutButton, helpButton;
    AdView mAdView;


    public static MenuFragment newInstance() {
        fragment = new MenuFragment();
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity=getActivity();
        activity.getWindow().getDecorView().setBackgroundColor(Color.WHITE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_menu, container, false);
        playButton = (RelativeLayout) view.findViewById(R.id.menu_play);
        statsButton = (RelativeLayout) view.findViewById(R.id.menu_stats);
        settingsButton = (RelativeLayout) view.findViewById(R.id.menu_settings);
        aboutButton = (RelativeLayout) view.findViewById(R.id.menu_about);
        helpButton = (RelativeLayout) view.findViewById(R.id.menu_help);

        mAdView = (AdView) view.findViewById(R.id.menu_ad);

        setOnClicks();

        loadAds();

        checkSignIn();

        return view;
    }

    private void loadAds(){

        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);
    }

    private void checkSignIn(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(MainActivity.preferences != null && !MainActivity.isUserSignedIn) {
                    if (!MainActivity.preferences.getBoolean("firstTimeUser", false)) {
                        showSignInDialog();
                        MainActivity.preferences.edit().putBoolean("firstTimeUser", true).apply();
                    }
                }
            }
        }, 2000);
    }

    private void showSignInDialog(){
        new AlertDialog.Builder(activity)
                .setTitle(R.string.login_title)
                .setMessage(R.string.login_msg)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Utils.launchFragment(activity, StatsFragment.newInstance(), StatsFragment.TAG);
                    }
                })
                .setNegativeButton(R.string.no, null)
                .show();
    }

    private void setOnClicks(){
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity.getApplicationContext(), GameActivity.class);
                intent.putExtra("unmuted", MainActivity.UNMUTED);
                activity.startActivity(intent);
            }
        });

        statsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.launchFragment(activity, StatsFragment.newInstance(), StatsFragment.TAG);
            }
        });

        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.launchFragment(activity, SettingsFragment.newInstance(), SettingsFragment.TAG);
            }
        });

        aboutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.launchFragment(activity, AboutFragment.newInstance(), AboutFragment.TAG);
            }
        });

        helpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.launchFragment(activity, HelpFragment.newInstance(), HelpFragment.TAG);
            }
        });

    }
}
