package com.sauradeeppaul.klink.speedtyper;

import android.content.Context;
import android.media.MediaPlayer;

/**
 * Created by sauradeep on 06/03/17.
 */
public class MusicManager {
    public enum Music {MUSIC_MENU, MUSIC_GAME, MUSIC_NONE}
    public enum FX {WRONG_MOVE}
    private static Music currentSong = Music.MUSIC_NONE;

    static MediaPlayer mp, fxmp;

    public static float FX_VOLUME = 100;
    public static float MUSIC_VOLUME = 100;

    public static void playAudioFX(Context context, FX fx){
        if (fx == FX.WRONG_MOVE && fxmp == null){
            fxmp = MediaPlayer.create(context, R.raw.wrong_move);
        }

        if (fxmp != null) {
            try {
                fxmp.setVolume(FX_VOLUME, FX_VOLUME);
                fxmp.start();
            } catch (Exception e) {
                e.printStackTrace();
                fxmp = MediaPlayer.create(context, R.raw.wrong_move);
            }
        }
    }

    public static void start(Context context, Music music) {

        if(music == currentSong){
            if(!isPlaying()) {
                try {
                    mp.start();
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
            return;
        }

        if (music == Music.MUSIC_GAME) {
            mp = MediaPlayer.create(context, R.raw.game_music);
            currentSong = Music.MUSIC_GAME;
        } else {
            mp = MediaPlayer.create(context, R.raw.menu_music);
            currentSong = Music.MUSIC_MENU;
        }

        if (mp != null) {
            try {
                mp.setVolume(MUSIC_VOLUME, MUSIC_VOLUME);
                mp.setLooping(true);
                mp.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public static void pause() {
        if (mp != null && mp.isPlaying()) {
            mp.pause();
        }
    }

    public static boolean isPlaying(){
        if (mp != null && mp.isPlaying()) {
            return true;
        }
        return false;
    }

    public static boolean isInitialized(){
        return mp != null;
    }

    public static void release() {
        try {
            if (mp != null) {
                if (mp.isPlaying()) {
                    mp.stop();
                }
                mp.release();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        currentSong = Music.MUSIC_NONE;
    }

    public static void releaseFxPlayer() {
        try {
            if (fxmp != null) {
                fxmp.release();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Music getCurrentSong() {
        return currentSong;
    }
}
