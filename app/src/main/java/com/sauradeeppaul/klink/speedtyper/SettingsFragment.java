package com.sauradeeppaul.klink.speedtyper;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by sauradeep on 07/03/17.
 */
public class SettingsFragment extends Fragment {

    static SettingsFragment fragment;
    public static final String TAG = "settings";

    static MainActivity activity;

    TextView muteButton, spaceAutoButton, fxButton;

    LinearLayout loggedInLL, loggedOutLL;

    public static SettingsFragment newInstance() {
        fragment = new SettingsFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(getActivity() instanceof MainActivity)
            activity=(MainActivity) getActivity();
        View view = inflater.inflate(R.layout.settings, container, false);

        muteButton = (TextView) view.findViewById(R.id.mute_unmute);
        fxButton = (TextView) view.findViewById(R.id.fx);
        spaceAutoButton = (TextView) view.findViewById(R.id.space_auto);

        addOnClickLinstener();
        updateMuteButton();
        updateFxButton();
        updateWordSwitchButton();

        return view;
    }

    private void addOnClickLinstener(){
        muteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleMuteButton();
            }
        });

        fxButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleFxButton();
            }
        });
        spaceAutoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleWordSwitchButton();
            }
        });
    }

    private void updateMuteButton(){
        if(MainActivity.UNMUTED)
            muteButton.setText(getResources().getString(R.string.mute));
        else
            muteButton.setText(getResources().getString(R.string.unmute));
    }

    private void updateFxButton(){
        if(MainActivity.UNMUTED_FX)
            fxButton.setText(getResources().getString(R.string.fx_mute));
        else
            fxButton.setText(getResources().getString(R.string.fx_unmute));
    }

    private void toggleMuteButton(){
        if(MainActivity.UNMUTED) {
            MainActivity.setUNMUTED(false);
            MusicManager.release();
        } else {
            MainActivity.setUNMUTED(true);
            MusicManager.start(activity, MusicManager.Music.MUSIC_MENU);
        }

        updateMuteButton();
    }

    private void toggleFxButton(){
        if(MainActivity.UNMUTED_FX) {
            MainActivity.setUNMUTED_FX(false);
            MusicManager.releaseFxPlayer();
        } else {
            MainActivity.setUNMUTED_FX(true);
        }

        updateFxButton();
    }

    private void toggleWordSwitchButton(){
        if(MainActivity.AUTO_SWITCH) {
            MainActivity.setAutoSwitch(false);
        } else {
            MainActivity.setAutoSwitch(true);
        }

        updateWordSwitchButton();
    }

    private void updateWordSwitchButton(){
        if(MainActivity.AUTO_SWITCH)
            spaceAutoButton.setText(getResources().getString(R.string.auto));
        else
            spaceAutoButton.setText(getResources().getString(R.string.space));
    }
}
