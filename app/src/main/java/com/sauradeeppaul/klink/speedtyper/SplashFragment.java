package com.sauradeeppaul.klink.speedtyper;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by sauradeep on 05/03/17.
 */
public class SplashFragment extends Fragment {

    static SplashFragment fragment;
    static MainActivity activity;

    public static final String TAG = "splash";

    ProgressBar bar;
    TextView loadedPerCent;

    LinearLayout loadingBlock, corruptBlock;
    TextView quitButton;

    int n;

    final static int FILE_SIZE = 3000;

    public static SplashFragment newInstance() {
        fragment = new SplashFragment();
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(getActivity() instanceof MainActivity)
            activity=(MainActivity) getActivity();
        View view = inflater.inflate(R.layout.splash, container, false);
        bar = (ProgressBar) view.findViewById(R.id.loading_bar);
        loadedPerCent = (TextView) view.findViewById(R.id.loaded_percent);
        bar.setMax(FILE_SIZE);
        bar.getProgressDrawable().setColorFilter(
                Color.WHITE, android.graphics.PorterDuff.Mode.SRC_IN);
        bar.setScaleY(1.5f);

        loadingBlock = (LinearLayout) view.findViewById(R.id.loading_block);
        corruptBlock = (LinearLayout) view.findViewById(R.id.corrupt_block);
        quitButton = (TextView) view.findViewById(R.id.quit);



        n = 0;

        startSplashScreen();

        return view;
    }

    public void startSplashScreen(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                MainActivity.words = new ArrayList<>();


                Scanner s = new Scanner(getResources().openRawResource(R.raw.quick_stream));

                String decodedString = "";
                try{
                    if(s.hasNext()) {
                        decodedString = decodedString + new String(Base64.decode(s.next(), Base64.DEFAULT));
                        s.close();

                        s = new Scanner(decodedString);

                        try {
                            while (s.hasNext()) {
                                MainActivity.words.add(s.next());
                                n++;

                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        bar.setProgress(n);
                                        loadedPerCent.setText(((n*100)/FILE_SIZE) + "%");
                                    }
                                });
                            }
                        } finally {
                            s.close();
                            if(n == FILE_SIZE)
                                Utils.launchFragment(activity, MenuFragment.newInstance(), MenuFragment.TAG);
                            else
                                handleError();
                        }

                    }

                } catch(Exception e){
                    e.printStackTrace();
                    handleError();
                }

            }
        }).start();
    }

    public void handleError(){
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                loadingBlock.setVisibility(View.GONE);
                corruptBlock.setVisibility(View.VISIBLE);

                quitButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        activity.finish();
                        System.exit(0);
                    }
                });
            }
        });
    }
}
