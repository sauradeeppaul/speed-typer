package com.sauradeeppaul.klink.speedtyper;

import android.support.v4.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.Games;

/**
 * Created by sauradeep on 05/03/17.
 */
public class StatsFragment extends Fragment {

    static StatsFragment fragment;
    public static final String TAG = "stats";

    static MainActivity activity;
    SignInButton signInButton;

    TextView welcomeText, wordsLeaderboardButton, lettersLeaderboardButton, signOutButton, achievementsButton;

    LinearLayout loggedInLL, loggedOutLL;

    public static StatsFragment newInstance() {
        fragment = new StatsFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(getActivity() instanceof MainActivity)
            activity=(MainActivity) getActivity();
        View view = inflater.inflate(R.layout.stats, container, false);

        signInButton = (SignInButton) view.findViewById(R.id.sign_in_button);

        loggedInLL = (LinearLayout) view.findViewById(R.id.user_logged_in_screen);
        loggedOutLL = (LinearLayout) view.findViewById(R.id.user_logged_out_screen);

        welcomeText = (TextView) view.findViewById(R.id.welcome);
        wordsLeaderboardButton = (TextView) view.findViewById(R.id.words_leaderboard);
        lettersLeaderboardButton = (TextView) view.findViewById(R.id.letters_leaderboard);
        achievementsButton = (TextView) view.findViewById(R.id.achievements);
        signOutButton = (TextView) view.findViewById(R.id.sign_out_button);

        handleSignInButtonBehaviour();

        handleUserButtonBehaviour();

        updateUI(activity.name, false);

        return view;
    }

    private void handleSignInButtonBehaviour(){
        signInButton.setSize(SignInButton.SIZE_WIDE);

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                activity.showProgressDialog();
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(MainActivity.mGoogleApiClient);
                activity.startActivityForResult(signInIntent, MainActivity.RC_SIGN_IN);
                MainActivity.mGoogleApiClient.connect(GoogleApiClient.SIGN_IN_MODE_OPTIONAL);
            }
        });
    }

    public void updateUI(String name, boolean animate){
        if(MainActivity.isUserSignedIn){
            showLoggedInScreen(name, animate);
        } else {
            showLoggedOutScreen(animate);
        }
    }

    private void handleUserButtonBehaviour(){


        wordsLeaderboardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.startActivityForResult(Games.Leaderboards.getLeaderboardIntent(MainActivity.mGoogleApiClient,
                        getResources().getString(R.string.word_leaderboard_id)), MainActivity.RC_LEADERBOARD);
            }
        });

        lettersLeaderboardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.startActivityForResult(Games.Leaderboards.getLeaderboardIntent(MainActivity.mGoogleApiClient,
                        getResources().getString(R.string.letter_leaderboard_id)), MainActivity.RC_LEADERBOARD);
            }
        });

        signOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.showProgressDialog();
                Auth.GoogleSignInApi.signOut(MainActivity.mGoogleApiClient).setResultCallback(
                        new ResultCallback<Status>() {
                            @Override
                            public void onResult(Status status) {
                                MainActivity.isUserSignedIn = false;
                                updateUI(null, true);
                                activity.hideProgressDialog();
                            }
                        });
            }
        });

        achievementsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.startActivityForResult(Games.Achievements.getAchievementsIntent(MainActivity.mGoogleApiClient),
                        MainActivity.RC_ACHIEVEMENTS);
            }
        });
    }

    private  void showLoggedInScreen(String name, boolean animate){

        switchScreen(loggedOutLL, loggedInLL, animate);
        if(name != null)
            welcomeText.setText("Hi, " + name + "!");
        else
            welcomeText.setText("Hello!");
    }

    private void showLoggedOutScreen(boolean animate){

        switchScreen(loggedInLL, loggedOutLL, animate);
    }

    private void switchScreen(final LinearLayout l1, final LinearLayout l2, boolean animate){
        if(animate) {
            Animation outAnimation = AnimationUtils.loadAnimation(activity, R.anim.dialog_exit);
            final Animation inAnimation = AnimationUtils.loadAnimation(activity, R.anim.dialog_entry);
            l2.clearAnimation();
            l2.setAnimation(inAnimation);

            l1.clearAnimation();
            l1.setAnimation(outAnimation);
            l1.startAnimation(outAnimation);
            outAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    l2.setVisibility(View.VISIBLE);
                    l1.setVisibility(View.GONE);
                    l2.startAnimation(inAnimation);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });
        } else {
            l2.setVisibility(View.VISIBLE);
            l1.setVisibility(View.GONE);
        }
    }
}
