package com.sauradeeppaul.klink.speedtyper;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by sauradeep on 05/03/17.
 */
public class TextLoader {

    public TextLoader(MainActivity activity) {
        //Read text from file

        List<String> words = new ArrayList<>();

        Scanner s = new Scanner(activity.getResources().openRawResource(R.raw.words));

        try {
            while (s.hasNext()) {
                words.add(s.next());
            }
        } finally {
            s.close();
        }
    }

}
