package com.sauradeeppaul.klink.speedtyper;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;

/**
 * Created by sauradeep on 04/03/17.
 */
public class Utils {

    public static void launchFragment (Activity act, Fragment fragment, String tag) {

        if(fragment instanceof MenuFragment) {
            FragmentTransaction transaction = ((MainActivity) act).getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.entry, R.anim.exit, R.anim.entry, R.anim.exit);
            transaction.replace(R.id.fragment_container, fragment, tag);
//            transaction.addToBackStack(MenuFragment.TAG);
            transaction.commit();
        }

        else if (fragment instanceof SplashFragment) {
            FragmentTransaction transaction = ((MainActivity) act).getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.fragment_container, fragment, tag);
//            transaction.addToBackStack(MenuFragment.TAG);
            transaction.commit();
        }

        else if(fragment instanceof StatsFragment || fragment instanceof SettingsFragment
                || fragment instanceof AboutFragment || fragment instanceof HelpFragment ){
            FragmentTransaction transaction = ((MainActivity) act).getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.frag_entry, R.anim.frag_exit, R.anim.frag_pop_entry, R.anim.frag_pop_exit);
            transaction.replace(R.id.fragment_container, fragment, tag);
            transaction.addToBackStack(tag);
            transaction.commit();
        }

        else if(fragment instanceof GameFragment) {
            FragmentTransaction transaction = ((GameActivity) act).getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.fragment_container, fragment, tag);
//            transaction.addToBackStack(GameFragment.TAG);
            transaction.commit();
        }
    }

    public static Fragment getCurrentFragment( ParentActivity act ) {

        if ( act == null )
            return null;
        FragmentManager fragmentManager = act.getSupportFragmentManager();
        if ( fragmentManager.getBackStackEntryCount() <= 0 ) return null;
        String fragmentTag = fragmentManager.getBackStackEntryAt( fragmentManager.getBackStackEntryCount() - 1 ).getName();
        Fragment currentFragment = act.getSupportFragmentManager()
                .findFragmentByTag( fragmentTag );

        return currentFragment;
    }
}
